//
//  ViewController.m
//  AudioRecordingDemo
//
//  Created by James Cash on 02-02-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
//#import <AVFoundation/AVFoundation.h>
@import AVFoundation;

@interface ViewController ()

@property (nonatomic,strong) NSURL *savedDirectory;
@property (nonatomic,strong) NSURL *fileURL;

@property (nonatomic,strong) AVAudioPlayer *player;
@property (nonatomic,strong) AVAudioRecorder *recorder;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.savedDirectory = [NSURL fileURLWithPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject]];
    NSLog(@"Document directory = %@", self.savedDirectory);
//    self.savedDirectory = [[NSFileManager defaultManager]
//                           URLForDirectory:NSDocumentDirectory
//                           inDomain:NSUserDomainMask
//                           appropriateForURL:nil
//                           create:NO
//                           error:nil];


// BEGIN BIG POINTER DIGRESSION
    NSMutableString *a = [@"hello" mutableCopy];
    [self foo:&a];
    NSLog(@"%@", a);

    NSInteger b = 5;
    [self bar:b];
    NSLog(@"%ld", b);
}

- (void)bar:(NSInteger)i
{
    i += 1;
}

- (void)foo:(NSMutableString**)s
{
    [*s appendString:@"!!!"];
    *s = [@"Foo" mutableCopy];
}
// END BIG POINTER DIGRESSION


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)toggleRecord:(UIButton *)sender {
    if ([self.recorder isRecording]) {
        [self.recorder stop];
        [sender setTitle:@"Record" forState:UIControlStateNormal];
        return;
    }

    [sender setTitle:@"Stop" forState:UIControlStateNormal];

    self.fileURL = [[self.savedDirectory
                    URLByAppendingPathComponent:[[NSUUID UUID] UUIDString]]
                    URLByAppendingPathExtension:@"mp4"];

    NSLog(@"Saving recording to %@", self.fileURL);

    NSError *err = nil;
    self.recorder = [[AVAudioRecorder alloc]
                     initWithURL:self.fileURL
                     settings:@{AVSampleRateKey: @(44100),
                                AVNumberOfChannelsKey: @(2),
                                AVFormatIDKey: @(kAudioFormatMPEG4AAC)}
                     error:&err];

    if (err != nil) {
        NSLog(@"Something went wrong %@", err.localizedDescription);
        abort();
    }
    [self.recorder record];
}

- (IBAction)togglePlay:(UIButton *)sender {
    if ([self.player isPlaying]) {
        [self.player stop];
        [sender setTitle:@"Play" forState:UIControlStateNormal];
        return;
    }
    [sender setTitle:@"Stop" forState:UIControlStateNormal];

    NSError *err = nil;
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:self.fileURL
                                                         error:&err];
    if (err != nil) {
        NSLog(@"Error creating player: %@", err.localizedDescription);
        abort();
    }
    [self.player play];
}

@end
